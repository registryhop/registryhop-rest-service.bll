﻿namespace RegistryHopRestService.BLL.Application.Accounts.Confirm.Services
{
    using RegistryHopRestService.BLL.Application.Accounts.Models;
    using RegistryHopRestService.BLL.Application.Accounts.Confirm.Models;

    public interface IAccountConfirmService
    {
        Account Confirm(AccountConfirmRequest accountCreateModel);
    }
}