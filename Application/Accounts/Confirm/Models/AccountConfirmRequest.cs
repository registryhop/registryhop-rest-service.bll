﻿namespace RegistryHopRestService.BLL.Application.Accounts.Confirm.Models
{
    using Newtonsoft.Json;

    public class AccountConfirmRequest
    {
        [JsonProperty(PropertyName = "confirmationCode")]
        public string ConfirmationCode { get; set; }
    }
}

