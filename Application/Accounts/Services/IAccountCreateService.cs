﻿namespace RegistryHopRestService.BLL.Application.Accounts.Services
{
    using RegistryHopRestService.BLL.Application.Accounts.Models;

    public interface IAccountCreateService
    {
        void Create(AccountCreateRequest accountCreateModel);
    }
}

