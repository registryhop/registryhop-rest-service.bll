﻿namespace RegistryHopRestService.BLL.Application.Accounts.Services
{
    using RegistryHopRestService.BLL.Application.Accounts.Models;
    using RegistryHopRestService.BLL.Application.Accounts.Exceptions;

    public class AccountCreateService : IAccountCreateService
    {
        const string InvalidEmailAddressErrorMessage = "Invalid email address.";

        public void Create(AccountCreateRequest accountCreateModel)
        {
            if (!accountCreateModel.Email.Equals("neil.opet@gmail.com"))
            {
                throw new AccountCreateException(InvalidEmailAddressErrorMessage);
            }
        }
    }
}

