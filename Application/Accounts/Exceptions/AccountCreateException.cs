﻿using System;

namespace RegistryHopRestService.BLL.Application.Accounts.Exceptions
{
    public class AccountCreateException : Exception
    {
        public AccountCreateException(string message)
            : base(message)
        {
        }

        public AccountCreateException(string message, Exception e)
            : base(message, e)
        {
        }
    }
}

