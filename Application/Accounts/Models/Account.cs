﻿namespace RegistryHopRestService.BLL.Application.Accounts.Models
{
    using Newtonsoft.Json;

    public class Account
    {
        [JsonProperty(PropertyName = "accountId")]
        public string AccountId
        {
            get;
            set;
        } = "";

        [JsonProperty(PropertyName = "name")]
        public string Name
        { 
            get;
            set; 
        } = "";

        [JsonProperty(PropertyName = "email")]
        public string Email
        { 
            get; 
            set; 
        } = "";

        [JsonProperty(PropertyName = "city")]
        public string City
        {
            get;
            set;
        } = "";

        [JsonProperty(PropertyName = "state")]
        public string State
        {
            get;
            set;
        } = "";
    }
}

