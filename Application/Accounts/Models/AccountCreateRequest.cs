﻿namespace RegistryHopRestService.BLL.Application.Accounts.Models
{
    using Newtonsoft.Json;

    public class AccountCreateRequest
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; } = "";
    }
}

